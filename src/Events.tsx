import app, { Component, on } from 'apprun';
import { IPairingEvents, events } from './api';
import fromNow from 'fromnow';

declare interface IState {
  events: Array<IPairingEvents>,
  loading: Boolean
}

export default class EventsComponent extends Component {
  state: IState = { events: [], loading: true }

  view = (state) => {
    return <div>
      {state.events.map(event => {
        const updated = fromNow(event.updated, { max: 1, suffix: true })
        return (<a href={'#/Event/' + event.id} className="card flex-md-row mb-4 shadow-sm h-md-250">
          <img className="card-img-left flex-auto d-none d-lg-block" alt="Event" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_166b7db80a7%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_166b7db80a7%22%3E%3Crect%20width%3D%22200%22%20height%3D%22250%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2256.203125%22%20y%3D%22131%22%3EEvent!%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 200px; height: 250px;" />
          <div className="card-body d-flex flex-column align-items-start">
            <h3 className="display-3 mb-0">
              <span className="text-dark">{event.name}</span>
            </h3>

            <div className="mb-1 text-muted">Last Updated: {updated}</div>
            <p className="card-text mb-auto">
              <strong>Round</strong>: {event.round}
            </p>
            <a>See pairings</a>
          </div>
        </a>);
      }
      )}

      <hr />
    </div >
  }

  updateState = async (state: IState) => {
    return {
      ...state,
      events: await events.all(),
      loading: false
    }
  }

  @on('#/') root = async (state) => await this.updateState(state)
}

