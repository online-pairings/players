window['defaultBasePath'] = '/backend';

import { get, post, del, put } from './fetch';
import { IPairingEvent } from './models';

export interface IPairingEvents {
    events: Array<IPairingEvent>;
}

export const events = {
    all: () => get<IPairingEvents>('/events'),
    get: (id) => get<IPairingEvent>('/events/' + id)
}