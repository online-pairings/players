import app from 'apprun';
import '../css/main.css'

app.on('#', (route, ...p) => {
  app.run(`#/${route || ''}`, ...p);
})

app.on('//', route => {
  const menus = document.querySelectorAll('.navbar-nav li');
  for (let i = 0; i < menus.length; ++i)
    menus[i].classList.remove('active');
  const item = document.querySelector(`[href='${route}']`);
  item && item.parentElement.classList.add('active');
})

const view = _ => <span>
  <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div className="container">
      <a className="navbar-brand" href="#">Pairings</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
        aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarResponsive">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <a className="nav-link" href="#About">About</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <main role="main" className="container" id="my-app" />
  <footer className="py-5 bg-dark">
    <div className="container">
      <p className="m-0 text-center text-white">
        <a className="text-muted" href="https://florius.com.ar">
          <span className="oi oi-heart" title="Con cariño" aria-hidden="true" alt="Con cariño" /> Florius
        </a>
      </p>
    </div>
  </footer>
</span>

app.start('main', {}, view, {})

import Events from './Events';
import Event from './Event';
import About from './About';

const element = 'my-app';
new Events().mount(element);
new Event().mount(element);
new About().mount(element);
