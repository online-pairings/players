export interface IPairingEvent {
    id: string;
    name: string;
    updated: Date;
}