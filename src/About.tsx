import app, { Component } from 'apprun';

export default class AboutComponent extends Component {
  state = 'About';

  view = (state) => {
    return <div>
      <h1>{state}</h1>
      <quote>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nibh ante, imperdiet tempus turpis in, sagittis aliquam justo. Etiam vel magna risus. Phasellus ultrices convallis diam, id vestibulum ligula luctus at. Aenean lectus mi, dictum non nibh in, malesuada imperdiet eros. Aenean sodales congue eros, vel convallis elit posuere sit amet. Suspendisse posuere volutpat est id feugiat. Vivamus a ipsum bibendum ante molestie dictum nec vitae leo. Praesent laoreet nibh non urna condimentum, sed ullamcorper lacus placerat. Vestibulum ornare augue quam, ac dictum dui scelerisque eget.
        Cras iaculis condimentum eros at placerat. Vestibulum ut odio ac lorem ultricies molestie nec malesuada sapien. Cras molestie et purus id vehicula. Duis ultricies sit amet elit vitae placerat. Nam et ipsum orci. Curabitur sed leo urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam vel sagittis nisl, nec commodo velit. Nunc diam arcu, ultrices vel odio eu, cursus efficitur lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis ex quam, eu sodales arcu scelerisque id. Nunc purus massa, volutpat non aliquam in, hendrerit quis orci. Pellentesque facilisis lectus urna, non porttitor nisi sagittis eu.
        Pellentesque venenatis, ex et eleifend consequat, metus lectus molestie lectus, et gravida justo ante ut ipsum. Cras ullamcorper sagittis nisl sit amet vestibulum. Aenean varius, nunc eu blandit varius, leo tortor bibendum dolor, non porttitor libero tellus id elit. Phasellus sem nunc, molestie quis vehicula commodo, consequat eget mi. Quisque ligula nunc, sollicitudin in eleifend at, rhoncus eu mauris. Maecenas ut auctor velit. Aliquam dictum massa vel posuere volutpat. Cras eros quam, consequat eget tortor in, bibendum porta metus. Nunc iaculis venenatis elementum. Vestibulum blandit massa quis diam ullamcorper posuere. Nam in dolor quis nunc tempor dapibus dignissim sit amet purus. Suspendisse orci enim, imperdiet id porta in, auctor vel metus. Etiam vel sodales elit. In congue, velit in ullamcorper accumsan, arcu ante iaculis sapien, a rutrum ipsum ante eu augue. Praesent sed diam ornare, rhoncus mauris id, blandit lorem.
        Cras elementum pretium leo ac efficitur. Aenean ut bibendum leo. In venenatis eros nisl, sit amet iaculis turpis ultrices ac. In dictum sollicitudin sem consequat tempus. Curabitur condimentum lectus ut risus aliquam blandit. Praesent sit amet diam accumsan, gravida est a, sollicitudin ipsum. Quisque pretium est id lectus porttitor euismod.
        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam non convallis odio, vitae posuere tortor. Integer convallis tincidunt lectus, quis luctus augue malesuada quis. Ut et lectus pulvinar, tristique enim vel, cursus magna. Nunc ac malesuada ligula. Maecenas tortor velit, vehicula sed mi vitae, placerat consequat nunc. Donec vitae tincidunt diam. Vivamus molestie ipsum id tortor auctor facilisis. Aenean suscipit, justo a fringilla rhoncus, nisi quam fermentum mauris, in vulputate orci purus eget velit. Sed mattis tortor nibh, a tristique est congue id. Quisque imperdiet faucibus lorem ut sollicitudin. Suspendisse varius dictum massa, sit amet dignissim augue accumsan eu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin fermentum enim ac quam aliquet aliquam a non velit. Fusce quis odio orci.
      </quote>
    </div>
  }

  update = {
    '#About': state => state,
  }
}
