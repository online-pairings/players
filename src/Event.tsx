import app, { Component, on } from 'apprun';
import { events } from './api';
import { IPairingEvent } from './models';

declare interface IState {
  event: IPairingEvent | void
  id: string | void
  loading: Boolean
}

export default class EventComponent extends Component {
  state: IState = { event: null, id: null, loading: true }

  view = (state) => {
    return <div>
      <h5 className="display-5">
        {state.name}
        <small>{state.id}</small>
      </h5>
    </div >
  }

  updateState = async (state: IState, id: string) => {
    return {
      ...state,
      event: await events.get(id),
      id: id
    }
  }

  @on('#/Event') root = async (state, id) => await this.updateState(state, id)
}

