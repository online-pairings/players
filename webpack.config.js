const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

module.exports = {
  entry: {
    'app': './src/main.tsx',
  },
  output: {
    filename: '[name].[hash].js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      { test: /.tsx?$/, use: 'ts-loader' },
      { test: /\.js$/, use: ['source-map-loader'], enforce: 'pre' },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/index.template.ejs',
      hash: false
    }),
    new ManifestPlugin(),
    new CopyWebpackPlugin([{ from: 'public', to: 'public' }])
  ],
  devServer: {
    open: true,
    contentBase: path.join(__dirname, 'dist'),
    publicPath: '',
    port: 3000,
    proxy: {
      '/backend': {
        target: 'http://localhost:8080',
        pathRewrite: { '^/backend': '' }
      }
    }
  },
  devtool: 'source-map'
}