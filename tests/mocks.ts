import app from 'apprun';
import { events } from '../src/api';

app.on('#', async (route, ...p) => {
    app.run(`#/${route || ''}`, ...p);
})


events.get = jest.fn((id) => ({ id: id }));