import app from 'apprun';
import Event from '../src/Event';
import { events } from '../src/api';
import './mocks';

const event = new Event().mount('my-app')

describe('home component', () => {

    it('should update state: #/Event/4', (done) => {
        app.run('route', '#/Event/4');
        setTimeout(() => {
            const state = event.state;
            expect(state.loading).toBe(true);
            expect(state.id).toBe("4");
            expect(state.event).toEqual({ id: "4" });
            expect(events.get).toHaveBeenCalledWith("4");
            done();
        })
    })

})